#!/usr/bin/env python3
#
# Copyright 2017 (c) Olliver Schinagl
# Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

import sys
from threading import Lock
import time
from time import sleep
from zeroconf import Zeroconf, ServiceBrowser, ServiceStateChange, ServiceInfo

lock = Lock()
service_adds = 0
service_removes = 0
retries = []
max_retries = 5

def add_service(zeroconf, service_type, name):
    # First try getting info from zeroconf cache
    info = ServiceInfo(service_type, name, properties = {})
    for record in zeroconf.cache.entries_with_name(name.lower()):
        info.update_record(zeroconf, time.time(), record)

    for record in zeroconf.cache.entries_with_name(info.server):
        info.update_record(zeroconf, time.time(), record)
        if info.address:
            break

    # Request more data if info is not complete
    retry = max_retries
    while retry > 0:
        if not info.address:
            info = zeroconf.get_service_info(service_type, name)

        if info:
            type_of_device = info.properties.get(b"type", None)
            if type_of_device:
                if type_of_device == b"printer":
                    address = '.'.join(map(lambda n: str(n), info.address))
                    global service_adds
                    lock.acquire()
                    service_adds += 1
                    if retry < 5:
                        retries.append(max_retries - retry)
                    lock.release()
                    return

        retry -= 1
    lock.acquire()
    retries.append(max_retries - retry)
    lock.release()

def remove_service(name):
    global service_removes
    lock.acquire()
    service_removes += 1
    lock.release()

def _onServiceChanged(zeroconf, service_type, name, state_change):
    if state_change is ServiceStateChange.Removed:
        remove_service(name)
    elif state_change is ServiceStateChange.Added:
        add_service(zeroconf, service_type, name)

def main(argc, argv):
    zeroconf = Zeroconf()
    browser = ServiceBrowser(zeroconf, "_ultimaker._tcp.local.", handlers=[_onServiceChanged])

    time.sleep(10)
    warn_retries = int(argv[1])
    err_retries = int(argv[1])
    zeroconf.close()

    tot_retries = 0
    if len(retries):
        for retry in retries:
            tot_retries += retry

    if tot_retries < warn_retries:
        log_level = 0
    elif tot_retries < err_retries:
        log_level = 1
    else:
        log_level = 2

    print("%d:%d:Services added: %d, services removed: %d, retries: %s" % (log_level, service_adds - service_removes, service_adds, service_removes, retries))

    return 0

if __name__ == "__main__":
    sys.exit(main(len(sys.argv), sys.argv))
